"use strict";

const { Ignitor } = require("@adonisjs/ignitor");
const https = require("https");
const fs = require("fs");

// Certificate
const options = {
  key: fs.readFileSync("/etc/letsencrypt/live/oneer.app/privkey.pem"),
  cert: fs.readFileSync("/etc/letsencrypt/live/oneer.app/fullchain.pem")
};

new Ignitor(require("@adonisjs/fold"))
  .appRoot(__dirname)
  .fireHttpServer(handler => {
    return https.createServer(options, handler);
  })
  // .fireHttpServer()
  .catch(console.error);
