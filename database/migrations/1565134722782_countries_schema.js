"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class CountriesSchema extends Schema {
  up() {
    this.create("countries", table => {
      table.increments();
      table.string("name");
      table.boolean("published").defaultTo(true);
      table.string("slug");
      table.string("image");
      table.timestamps();
    });
  }

  down() {
    this.drop("countries");
  }
}

module.exports = CountriesSchema;
