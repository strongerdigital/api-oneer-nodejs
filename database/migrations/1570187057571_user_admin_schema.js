'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserAdminSchema extends Schema {
  up () {
    this.alter('users', (table) => {
      table.boolean("admin").defaultTo(false)
    })
  }

  down () {
    this.drop('user_admins')
  }
}

module.exports = UserAdminSchema
