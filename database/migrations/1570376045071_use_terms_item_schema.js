"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class UseTermsItemSchema extends Schema {
  up() {
    this.create("use_terms_items", table => {
      table.increments();
      table.string("title");
      table.text("description");
      table.timestamps();
    });
  }

  down() {
    this.drop("use_terms_items");
  }
}

module.exports = UseTermsItemSchema;
