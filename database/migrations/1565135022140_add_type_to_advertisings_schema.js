"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class AddTypeToAdvertisingsSchema extends Schema {
  up() {
    this.alter("advertisings", table => {
      table.string("advertising_type");
    });
  }

  down() {
    this.drop("add_type_to_advertisings");
  }
}

module.exports = AddTypeToAdvertisingsSchema;
