"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class AddResendAdvertisingFieldToAdvertisingsSchema extends Schema {
  up() {
    this.alter("advertisings", table => {
      table.boolean("resend_advertising");
    });
  }

  down() {
    this.drop("add_resend_advertising_field_to_advertisings");
  }
}

module.exports = AddResendAdvertisingFieldToAdvertisingsSchema;
