"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class QuestionsSchema extends Schema {
  up() {
    this.create("questions", table => {
      table.increments();
      table.string("title");
      table.text("description");
      table.boolean("published");
      table.timestamps();
    });
  }

  down() {
    this.drop("questions");
  }
}

module.exports = QuestionsSchema;
