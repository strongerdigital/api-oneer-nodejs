"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class AdvertisingLocationsSchema extends Schema {
  up() {
    this.create("advertising_locations", table => {
      table.increments();
      table
        .integer("advertising_id")
        .references()
        .inTable("advertisings");
      table
        .integer("user_id")
        .references()
        .inTable("users");
      table.timestamp("date_init");
      table.timestamp("date_end");
      table.decimal("price");
      table.decimal("price_total");
      table.boolean("paid");
      table.string("status").defaultTo("reserved");
      table.string("payment_status").defaultTo("waiting");
      table.text("observations");
      table.integer("occupants");
      table.string("reservation_code");
      table.string("reservation_number");
      table.string("slug");
      table.timestamps();
    });
  }

  down() {
    this.drop("advertising_locations");
  }
}

module.exports = AdvertisingLocationsSchema;
