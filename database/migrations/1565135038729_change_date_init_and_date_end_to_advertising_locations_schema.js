'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChangeDateInitAndDateEndToAdvertisingLocationsSchema extends Schema {
  up () {
    this.create('change_date_init_and_date_end_to_advertising_locations', (table) => {
      table.increments()
      table.timestamps()
    })
  }

  down () {
    this.drop('change_date_init_and_date_end_to_advertising_locations')
  }
}

module.exports = ChangeDateInitAndDateEndToAdvertisingLocationsSchema
