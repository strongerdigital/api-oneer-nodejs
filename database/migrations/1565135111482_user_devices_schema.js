"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class UserDevicesSchema extends Schema {
  up() {
    this.create("user_devices", table => {
      table.increments();
      table
        .integer("user_id")
        .references()
        .inTable("users");
      table.text("token");
      table.boolean("mute");
      table.integer("mute_durantion");
      table.timestamp("mute_start");
      table.boolean("android");
      table.timestamps();
    });
  }

  down() {
    this.drop("user_devices");
  }
}

module.exports = UserDevicesSchema;
