"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class AdvertisingAccessoriesSchema extends Schema {
  up() {
    this.create("advertising_accessories", table => {
      table.increments();
      table
        .integer("advertising_id")
        .references()
        .inTable("advertisings");
      table
        .integer("accessory_id")
        .references()
        .inTable("accessorys");
      table.timestamps();
    });
  }

  down() {
    this.drop("advertising_accessories");
  }
}

module.exports = AdvertisingAccessoriesSchema;
