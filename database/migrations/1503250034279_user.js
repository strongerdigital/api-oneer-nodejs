"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class UserSchema extends Schema {
  up() {
    this.create("users", table => {
      table.increments();
      table.string("name", 80).notNullable();
      table.boolean("master").defaultTo(false);
      table.boolean("active").defaultTo(true);

      table.string("identifier");
      table.string("register");
      table.string("phone");
      table.string("mobile");
      table.string("street");
      table.string("number");
      table.string("zipcode");
      table.string("neigborhood");
      table.integer("city_id");
      table
        .string("email", 254)
        .notNullable()
        .unique();
      table.string("password", 60).notNullable();
      table.string("slug");
      table.string("image");
      table.string("reset_password_token").unique();
      table.timestamp("reset_password_sent_at");
      table.timestamp("confirmation_token").unique();
      table.timestamps();
    });
  }

  down() {
    this.drop("users");
  }
}

module.exports = UserSchema;
