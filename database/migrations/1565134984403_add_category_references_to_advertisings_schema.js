"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class AddCategoryReferencesToAdvertisingsSchema extends Schema {
  up() {
    this.alter("advertisings", table => {
      table
        .integer("category_id")
        .references()
        .inTable("categories");
    });
  }

  down() {
    this.drop("add_category_references_to_advertisings");
  }
}

module.exports = AddCategoryReferencesToAdvertisingsSchema;
