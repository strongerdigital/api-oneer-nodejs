"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class AdvertisingEvaluationsSchema extends Schema {
  up() {
    this.create("advertising_evaluations", table => {
      table.increments();
      table
        .integer("advertising_id")
        .references()
        .inTable("advertisings");
      table
        .integer("user_id")
        .references()
        .inTable("users");
      table.integer("grade");
      table.text("comment");
      table.timestamps();
    });
  }

  down() {
    this.drop("advertising_evaluations");
  }
}

module.exports = AdvertisingEvaluationsSchema;
