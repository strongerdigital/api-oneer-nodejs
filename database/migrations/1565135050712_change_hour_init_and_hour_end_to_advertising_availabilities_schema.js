'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChangeHourInitAndHourEndToAdvertisingAvailabilitiesSchema extends Schema {
  up () {
    this.create('change_hour_init_and_hour_end_to_advertising_availabilities', (table) => {
      table.increments()
      table.timestamps()
    })
  }

  down () {
    this.drop('change_hour_init_and_hour_end_to_advertising_availabilities')
  }
}

module.exports = ChangeHourInitAndHourEndToAdvertisingAvailabilitiesSchema
