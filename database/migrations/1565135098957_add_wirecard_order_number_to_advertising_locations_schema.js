"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class AddWirecardOrderNumberToAdvertisingLocationsSchema extends Schema {
  up() {
    this.alter("advertising_locations", table => {
      table.string("wirecard_order_number");
    });
  }

  down() {
    this.drop("add_wirecard_order_number_to_advertising_locations");
  }
}

module.exports = AddWirecardOrderNumberToAdvertisingLocationsSchema;
