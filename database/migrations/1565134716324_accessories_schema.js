"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class AccessoriesSchema extends Schema {
  up() {
    this.create("accessories", table => {
      table.increments();
      table.string("name");
      table.text("description");
      table.string("slug");
      table.string("image");
      table.boolean("published").defaultTo(true);
      table.timestamps();
    });
  }

  down() {
    this.drop("accessories");
  }
}

module.exports = AccessoriesSchema;
