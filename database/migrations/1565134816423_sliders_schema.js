"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class SlidersSchema extends Schema {
  up() {
    this.create("sliders", table => {
      table.increments();
      table.string("title");
      table.string("link");
      table.string("target");
      table.string("image");
      table.string("image_mobile");
      table.boolean("published").defaultTo(true);
      table.string("slug");
      table.timestamps();
    });
  }

  down() {
    this.drop("sliders");
  }
}

module.exports = SlidersSchema;
