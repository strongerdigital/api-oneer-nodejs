"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class StatesSchema extends Schema {
  up() {
    this.create("states", table => {
      table.increments();
      table.string("name");
      table.boolean("published").defaultTo(true);
      table.string("slug");
      table.string("image");
      table.timestamps();
    });
  }

  down() {
    this.drop("states");
  }
}

module.exports = StatesSchema;
