"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class AddRejectedFieldToAdvertisingsSchema extends Schema {
  up() {
    this.alter("advertisings", table => {
      table.boolean("rejected");
    });
  }

  down() {
    this.drop("add_rejected_field_to_advertisings");
  }
}

module.exports = AddRejectedFieldToAdvertisingsSchema;
