"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class AddOccupantsNumberToAdvertisingsSchema extends Schema {
  up() {
    this.alter("advertisings", table => {
      table.integer("occupants");
    });
  }

  down() {
    this.drop("add_occupants_number_to_advertisings");
  }
}

module.exports = AddOccupantsNumberToAdvertisingsSchema;
