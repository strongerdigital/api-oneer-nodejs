"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class AddWorkpassFieldToAdvertisingsSchema extends Schema {
  up() {
    this.alter("advertisings", table => {
      table.boolean("workpass");
    });
  }

  down() {
    this.drop("add_workpass_field_to_advertisings");
  }
}

module.exports = AddWorkpassFieldToAdvertisingsSchema;
