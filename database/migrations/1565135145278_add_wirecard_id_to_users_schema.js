"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class AddWirecardIdToUsersSchema extends Schema {
  up() {
    this.alter("users", table => {
      table.string("wirecard_id");
    });
  }

  down() {
    this.drop("add_wirecard_id_to_users");
  }
}

module.exports = AddWirecardIdToUsersSchema;
