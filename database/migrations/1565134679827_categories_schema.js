"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class CategoriesSchema extends Schema {
  up() {
    this.create("categories", table => {
      table.increments();
      table.string("title");
      table.string("slug");
      table.string("image");
      table.boolean("published").defaultTo(true);
      table.timestamps();
    });
  }

  down() {
    this.drop("categories");
  }
}

module.exports = CategoriesSchema;
