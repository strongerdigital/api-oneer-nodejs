"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class AddDocumentAttechToAdsSchema extends Schema {
  up() {
    this.alter("advertisings", table => {
      table.string("first_document");
      table.string("second_document");
    });
  }

  down() {
    this.drop("add_document_attech_to_ads");
  }
}

module.exports = AddDocumentAttechToAdsSchema;
