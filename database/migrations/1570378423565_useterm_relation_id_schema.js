"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class UsetermRelationIdSchema extends Schema {
  up() {
    this.alter("use_terms_items", table => {
      table.integer("use_term_id");
    });
  }

  down() {
    this.drop("useterm_relation_ids");
  }
}

module.exports = UsetermRelationIdSchema;
