"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class AdvertisingLocationMessagesSchema extends Schema {
  up() {
    this.create("advertising_location_messages", table => {
      table.increments();
      table
        .integer("advertising_id")
        .references()
        .inTable("advertisings");
      table
        .integer("advertising_location_id")
        .references()
        .inTable("advertising_locations");
      table
        .integer("user_id")
        .references()
        .inTable("users");
      table.text("message");
      table.timestamps();
    });
  }

  down() {
    this.drop("advertising_location_messages");
  }
}

module.exports = AdvertisingLocationMessagesSchema;
