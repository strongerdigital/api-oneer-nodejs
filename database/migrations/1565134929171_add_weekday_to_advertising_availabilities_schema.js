"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class AddWeekdayToAdvertisingAvailabilitiesSchema extends Schema {
  up() {
    this.alter("advertising_availabilities", table => {
      table.integer("weekday");
    });
  }

  down() {
    this.drop("add_weekday_to_advertising_availabilities");
  }
}

module.exports = AddWeekdayToAdvertisingAvailabilitiesSchema;
