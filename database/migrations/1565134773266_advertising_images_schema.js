"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class AdvertisingImagesSchema extends Schema {
  up() {
    this.create("advertising_images", table => {
      table.increments();
      table
        .integer("advertising_id")
        .references()
        .inTable("advertising");
      table.text("caption");
      table.string("image");
      table.timestamps();
    });
  }

  down() {
    this.drop("advertising_images");
  }
}

module.exports = AdvertisingImagesSchema;
