"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class AddHourInitAndHourEndFieldsToAdvertisingLocationsSchema extends Schema {
  up() {
    this.alter("advertising_locations", table => {
      table.integer("hour_init");
      table.integer("hour_end");
    });
  }

  down() {
    this.drop("add_hour_init_and_hour_end_fields_to_advertising_locations");
  }
}

module.exports = AddHourInitAndHourEndFieldsToAdvertisingLocationsSchema;
