"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class TaxesSchema extends Schema {
  up() {
    this.create("taxes", table => {
      table.increments();
      table.decimal("string");
      table.timestamps();
    });
  }

  down() {
    this.drop("taxes");
  }
}

module.exports = TaxesSchema;
