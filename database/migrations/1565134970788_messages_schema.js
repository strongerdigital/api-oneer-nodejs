"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class MessagesSchema extends Schema {
  up() {
    this.create("messages", table => {
      table.increments();
      table.string("name");
      table.string("phone");
      table.string("email");
      table.string("subject");
      table.text("text");
      table.string("status").defaultTo("waiting");
      table.string("slug");
      table.timestamps();
    });
  }

  down() {
    this.drop("messages");
  }
}

module.exports = MessagesSchema;
