"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class AdvertisingAvailabilitiesSchema extends Schema {
  up() {
    this.create("advertising_availabilities", table => {
      table.increments();
      table
        .integer("advertising_id")
        .references()
        .inTable("advertisings");
      table.timestamp("day");
      table.integer("hour_init");
      table.integer("hour_end");
      table.decimal("price");
      table.timestamps();
    });
  }

  down() {
    this.drop("advertising_availabilities");
  }
}

module.exports = AdvertisingAvailabilitiesSchema;
