"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class SubcategoriesSchema extends Schema {
  up() {
    this.create("subcategories", table => {
      table.increments();
      table.string("title");
      table
        .integer("category_id")
        .references()
        .inTable("categories");
      table.boolean("published").defaultTo(true);
      table.string("slug");
      table.string("image");

      table.timestamps();
    });
  }

  down() {
    this.drop("subcategories");
  }
}

module.exports = SubcategoriesSchema;
