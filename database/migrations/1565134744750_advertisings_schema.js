"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class AdvertisingsSchema extends Schema {
  up() {
    this.create("advertisings", table => {
      table.increments();
      table
        .integer("user_id")
        .references()
        .inTable("users");
      table.text("title");
      table.text("description");
      table.text("rules");
      table.string("neighborhood");
      table.string("street");
      table.string("number");
      table.string("zipcode");
      table.string("complement");
      table.string("phone");
      table.string("phone_second");
      table.text("observation");
      table.text("map_url");
      table.string("lat");
      table.string("lng");

      table
        .integer("city_id")
        .references()
        .inTable("cities");
      table.decimal("price");
      table.boolean("published").defaultTo(false);
      table.string("reason_for_rejection");
      table.text("rejection_text");
      table.string("slug");

      table.timestamps();
    });
  }

  down() {
    this.drop("advertisings");
  }
}

module.exports = AdvertisingsSchema;
