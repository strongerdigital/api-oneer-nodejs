"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class AddQuantitySpacesToAdvertisingsSchema extends Schema {
  up() {
    this.alter("advertisings", table => {
      table.integer("quantity_spaces");
    });
  }

  down() {
    this.drop("add_quantity_spaces_to_advertisings");
  }
}

module.exports = AddQuantitySpacesToAdvertisingsSchema;
