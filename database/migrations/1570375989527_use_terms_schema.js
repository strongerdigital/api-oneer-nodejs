"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class UseTermsSchema extends Schema {
  up() {
    this.create("use_terms", table => {
      table.increments();
      table.text("description");
      table.timestamps();
    });
  }

  down() {
    this.drop("use_terms");
  }
}

module.exports = UseTermsSchema;
