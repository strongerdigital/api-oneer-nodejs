"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class CitiesSchema extends Schema {
  up() {
    this.create("cities", table => {
      table.increments();
      table
        .integer("state_id")
        .references()
        .inTable("states");
      table.boolean("published").defaultTo(true);
      table.string("slug");
      table.string("image");
      table.timestamps();
    });
  }

  down() {
    this.drop("cities");
  }
}

module.exports = CitiesSchema;
