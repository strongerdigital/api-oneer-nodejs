"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class AddFildsToUsersSchema extends Schema {
  up() {
    this.alter("users", table => {
      table.string("gender");
      table.timestamp("birthday");
      table.string("register_organ");
      table.string("state_register");
      table.string("cnh_number");
      table.timestamp("cnh_validate");
      table.string("nationality");
      table.string("country");
    });
  }

  down() {
    this.drop("add_filds_to_users");
  }
}

module.exports = AddFildsToUsersSchema;
