"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class AddCaptionFieldToSlidersSchema extends Schema {
  up() {
    this.alter("sliders", table => {
      table.string("caption");
    });
  }

  down() {
    this.drop("add_caption_field_to_sliders");
  }
}

module.exports = AddCaptionFieldToSlidersSchema;
