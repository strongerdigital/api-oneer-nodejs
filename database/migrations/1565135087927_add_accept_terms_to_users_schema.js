"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class AddAcceptTermsToUsersSchema extends Schema {
  up() {
    this.alter("users", table => {
      table.boolean("accept_terms");
    });
  }

  down() {
    this.drop("add_accept_terms_to_users");
  }
}

module.exports = AddAcceptTermsToUsersSchema;
