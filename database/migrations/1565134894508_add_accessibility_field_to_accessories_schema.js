"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class AddAccessibilityFieldToAccessoriesSchema extends Schema {
  up() {
    this.alter("accessories", table => {
      table.boolean("accessibility");
    });
  }

  down() {
    this.drop("add_accessibility_field_to_accessories");
  }
}

module.exports = AddAccessibilityFieldToAccessoriesSchema;
