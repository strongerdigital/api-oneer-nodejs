"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class FavouritesSchema extends Schema {
  up() {
    this.create("favourites", table => {
      table.increments();
      table
        .integer("advertising_id")
        .references()
        .inTable("advertising");
      table
        .integer("user_id")
        .references()
        .inTable("users");
      table.timestamps();
    });
  }

  down() {
    this.drop("favourites");
  }
}

module.exports = FavouritesSchema;
