"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class AddWorkpassFieldToUsersSchema extends Schema {
  up() {
    this.alter("users", table => {
      table.boolean("workpass");
    });
  }

  down() {
    this.drop("add_workpass_field_to_users");
  }
}

module.exports = AddWorkpassFieldToUsersSchema;
