"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class AddAcceptTermsFieldToAdvertisingLocationsSchema extends Schema {
  up() {
    this.alter("advertising_locations", table => {
      table.boolean("accept_terms");
    });
  }

  down() {
    this.drop("add_accept_terms_field_to_advertising_locations");
  }
}

module.exports = AddAcceptTermsFieldToAdvertisingLocationsSchema;
