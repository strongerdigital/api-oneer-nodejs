"use strict";

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use("Model");

class UseTerm extends Model {
  items() {
    return this.hasMany("App/Models/UseTermsItem");
  }
}

module.exports = UseTerm;
