"use strict";

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use("Model");

class Advertising extends Model {
  city() {
    return this.belongsTo("App/Models/City");
  }
  availabilities() {
    return this.hasMany("App/Models/AdvertisingAvailability");
  }
  photos() {
    return this.hasMany("App/Models/AdvertisingImage");
  }

  accessories() {
    return this.belongsToMany("App/Models/Accessory").pivotModel(
      "App/Models/AdvertisingAccessory"
    );
    // .withPivot([]);
  }
}

module.exports = Advertising;
