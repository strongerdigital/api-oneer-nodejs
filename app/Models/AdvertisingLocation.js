"use strict";

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use("Model");

class AdvertisingLocation extends Model {
  advertising() {
    return this.belongsTo("App/Models/Advertising");
  }
}

module.exports = AdvertisingLocation;
