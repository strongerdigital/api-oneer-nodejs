"use strict";
const Model = use("App/Models/City");
const Env = use("Env");

class CityController {
  async index() {
    try {
      const model = await Model.query()
        .with("state")
        .fetch();
      return model;
    } catch (error) {
      return { error };
    }
  }
  async store({ request }) {
    try {
      const data = request.all();
      const model = await Model.create(data);
      return model;
    } catch (error) {
      return { error };
    }
  }
  async show({ params }) {
    try {
      const { id } = params;
      const model = await Model.query()
        .where("slug", id)

        .first();
      return model;
    } catch (error) {
      return { error };
    }
  }
  async update({ params, request }) {
    try {
      const { id } = params;
      const data = request.all();
      const model = await Model.find(id);
      await model.merge(data);
      await model.save();
      return model;
    } catch (error) {
      return { error };
    }
  }
  async search({ params, request }) {
    try {
      const env = Env.get("DB_CONNECTION");
      const like = env == "sqlite" ? "like" : "iLike";
      const { name, uf } = params;
      const model = await Model.query()
        .where("slug", like, "%" + name.toUpperCase() + "%")
        .whereHas("state", builder => {
          builder.where("slug", uf);
        })
        .first();

      return model;
    } catch (error) {
      return { error };
    }
  }
}

module.exports = CityController;
