"use strict";
const Helpers = use("Helpers");
const Crypto = require("crypto");

const { format, getDay, differenceInMinutes } = require("date-fns");

const Model = use("App/Models/Advertising");
const AdvertisingAccessory = use("App/Models/AdvertisingAccessory");
const AdvertisingAvailability = use("App/Models/AdvertisingAvailability");
const AdvertisingImage = use("App/Models/AdvertisingImage");

const AdvertisingService = use("App/Services/AdvertisingService");

const Env = use("Env");

class AdvertisingController {
  async index() {
    try {
      const model = await Model.query()
        .with("city.state")
        .with("accessories")
        .with("availabilities")
        .with("photos")
        .fetch();
      return model;
    } catch (error) {
      return { error };
    }
  }
  async featured() {
    try {
      const model = await Model.query()
        .orderBy("created_at", "DESC")
        .with("city.state")
        .with("accessories")
        .with("availabilities")
        .with("photos")
        .fetch();
      return model;
    } catch (error) {
      return { error };
    }
  }
  async last4() {
    try {
      const model = await Model.query()
        .orderBy("created_at", "DESC")
        .with("city.state")
        .with("accessories")
        .with("availabilities")
        .with("photos")
        .limit(3)
        .fetch();
      return model;
    } catch (error) {
      return { error };
    }
  }
  async search({ request }) {
    try {
      const { term } = request.all();
      const env = Env.get("DB_CONNECTION");
      const like = env == "sqlite" ? "like" : "iLike";
      const model = await Model.query()
        .where("title", like, "%" + term.toUpperCase() + "%")
        .orWhere("title", like, term.toUpperCase() + "%")
        .orWhere("neighborhood", like, term.toUpperCase() + "%")
        .orWhere("neighborhood", like, "%" + term.toUpperCase() + "%")
        .orWhere("street", like, term.toUpperCase() + "%")
        .orWhere("street", like, "%" + term.toUpperCase() + "%")
        .orderBy("title", "ASC")
        .orWhereHas("city", builder => {
          builder.where("slug", like, "%" + term.toUpperCase() + "%");
        })
        .with("city.state")
        .with("accessories")
        .with("availabilities")
        .with("photos")
        .fetch();
      return model;
    } catch (error) {
      console.log(error);
      return { error };
    }
  }
  async store({ request }) {
    try {
      const validationOptions = {
        size: "2mb",
        extnames: ["png", "gif", "jpg", "jpeg", "pdf"]
      };
      const { accessories, availabilities, city } = request.all();

      const data = request.except([
        "accessories",
        "availabilities",
        "city",
        "state",
        "country_id",
        "first_document",
        "second_document"
      ]);

      const model = await Model.create(data);

      if (accessories) {
        const acs = JSON.parse(accessories);
        for (const [idx, element] of acs.entries()) {
          await AdvertisingAccessory.findOrCreate({
            advertising_id: model.toJSON().id,
            accessory_id: element
          });
        }
      }
      if (availabilities) {
        const avs = JSON.parse(availabilities);
        for (const [idx, element] of avs.entries()) {
          await AdvertisingAvailability.findOrCreate({
            advertising_id: model.toJSON().id,
            weekday: element.weekday,
            hour_init: format(element.hour_init, "HH:mm"),
            hour_end: format(element.hour_end, "HH:mm")
          });
        }
      }

      if (model.toJSON().id) {
        const first_document = request.file(
          "first_document",
          validationOptions
        );
        const second_document = request.file(
          "second_document",
          validationOptions
        );
        if (first_document) {
          let randname = await Crypto.randomBytes(20).toString("hex");

          await first_document.move(
            Helpers.publicPath(`documents/${model.toJSON().id}`),
            {
              name: randname + "." + first_document.extname,
              overwrite: true
            }
          );

          await model.merge({
            first_document: `documents/${model.toJSON().id}/${
              first_document.fileName
            }`
          });
        }
        if (second_document) {
          let randname = await Crypto.randomBytes(20).toString("hex");

          await second_document.move(
            Helpers.publicPath(`documents/${model.toJSON().id}`),
            {
              name: randname + "." + second_document.extname,
              overwrite: true
            }
          );

          await model.merge({
            second_document: `documents/${model.toJSON().id}/${
              second_document.fileName
            }`
          });
        }
        await model.save();
      }

      const slug = await AdvertisingService.createSlug(model.toJSON());
      await model.merge({
        slug
      });
      await model.save();

      const { state, number, street } = request.all();
      const geocode = await AdvertisingService.geocode({
        city,
        state,
        number,
        street
      });
      await model.merge(geocode);
      await model.save();

      return model;
    } catch (error) {
      console.log(error);
      return { error };
    }
  }
  async show({ params }) {
    try {
      const { id } = params;
      const model = await Model.query()
        .where("slug", id)

        .with("city.state")
        .with("accessories")
        .with("availabilities")
        .with("photos")
        .first();
      return model;
    } catch (error) {
      return { error };
    }
  }
  async update({ params, request }) {
    try {
      const { id } = params;
      const data = request.all();
      const model = await Model.find(id);
      await model.merge(data);
      await model.save();
      return model;
    } catch (error) {
      return { error };
    }
  }
  async createAccessory({ params, request }) {
    try {
      const advertising_id = params.id;
      const { accessory_id } = request.all();
      const model = await AdvertisingAccessory.findOrCreate({
        advertising_id,
        accessory_id
      });

      return model;
    } catch (error) {
      return { error };
    }
  }
  async availability({ params, request }) {
    try {
      const advertising_id = params.id;
      const {
        date_init,
        date_end,
        hour_init,
        hour_end,
        occupants
      } = request.all();
      let error = null;

      const model = await Model.query()
        .where("id", advertising_id)
        .whereHas("availabilities", builder => {
          builder.where("weekday", "<=", getDay(date_init));
          builder.where("weekday", ">=", getDay(date_end));
          builder.where("hour_init", "<=", format(hour_init, "HH:mm"));
          builder.where("hour_end", ">=", format(hour_end, "HH:mm"));
        })
        .first();

      if (date_init > date_end) {
        error = "A data de início é superior ao término";
      } else if (hour_init > hour_end) {
        error = "A hora de início é superior ao término";
      } else {
      }

      if (error) {
        return {
          error
        };
      } else {
        if (model) {
          const diffHours =
            parseFloat(differenceInMinutes(hour_end, hour_init)) / 60;

          return {
            success: true,
            ...model.toJSON(),
            price_total:
              parseFloat(diffHours) * parseFloat(model.toJSON().price),
            price: parseFloat(diffHours) * parseFloat(model.toJSON().price),
            date_end: `${format(date_end, "DD/MM/YYYY")} ${format(
              hour_end,
              "HH:mm"
            )}`,
            date_init: `${format(date_init, "DD/MM/YYYY")} ${format(
              hour_init,
              "HH:mm"
            )}`
          };
        } else {
          return { error: "Não existe horário disponível para sua busca." };
        }
      }
    } catch (error) {
      console.log(error);
      return { error: "Favor preencher todos os campos" };
    }
  }
  async uploadPhoto({ params, request, response }) {
    const validationOptions = {
      types: ["image"],
      size: "2mb",
      extnames: ["png", "gif", "jpg", "jpeg"]
    };

    const photo = request.file("photo", validationOptions);

    const randname = await Crypto.randomBytes(20).toString("hex");
    await photo.move(Helpers.publicPath(`photos/${params.id}`), {
      name: randname + ".jpg",
      overwrite: true
    });

    await AdvertisingImage.create({
      advertising_id: params.id,
      image: `photos/${params.id}/${photo.fileName}`
    });

    return { sucess: true, fileName: `photos/${params.id}/${photo.fileName}` };
  }
}

module.exports = AdvertisingController;
