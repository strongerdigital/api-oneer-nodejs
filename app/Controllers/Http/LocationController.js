"use strict";
const Model = use("App/Models/AdvertisingLocation");
const Env = use("Env");

class LocationController {
  async index() {
    try {
      const model = await Model.query().fetch();
      return model;
    } catch (error) {
      return { error };
    }
  }
  async store({ request }) {
    try {
      const data = request.all();
      const model = await Model.create(data);
      return model;
    } catch (error) {
      return { error };
    }
  }
  async show({ params }) {
    try {
      const { id } = params;
      const model = await Model.query()
        .where("id", id)

        .first();
      return model;
    } catch (error) {
      return { error };
    }
  }
  async update({ params, request }) {
    try {
      const { id } = params;
      const data = request.all();
      const model = await Model.find(id);
      await model.merge(data);
      await model.save();
      return model;
    } catch (error) {
      return { error };
    }
  }
  async search({ params, request }) {
    try {
      const env = Env.get("DB_CONNECTION");
      const like = env == "sqlite" ? "like" : "iLike";
      const { name, uf } = params;
      const model = await Model.query()
        .where("slug", like, "%" + name.toUpperCase() + "%")
        .whereHas("state", builder => {
          builder.where("slug", uf);
        })
        .first();

      return model;
    } catch (error) {
      return { error };
    }
  }
}

module.exports = LocationController;
