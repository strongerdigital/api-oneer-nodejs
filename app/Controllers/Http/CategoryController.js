"use strict";
const Model = use("App/Models/Category");
const Helpers = use("Helpers");
const Crypto = require("crypto");
const slugify = require("slugify");
class CategoryController {
  async index() {
    try {
      const model = await Model.query().fetch();
      return model;
    } catch (error) {
      return { error };
    }
  }
  async store({ request }) {
    try {
      const data = request.except(["photo"]);
      const model = await Model.create(data);
      const slug = slugify(data.title, {
        replacement: "-", // replace spaces with replacement
        remove: null, // regex to remove characters
        lower: true // result in lower case
      });

      return model;
    } catch (error) {
      console.log(error);
      return { error };
    }
  }
  async show({ params }) {
    try {
      const { id } = params;
      const model = await Model.query()
        .where("slug", id)
        .orWhere("id", id)
        .with("places", builder => {
          builder
            .with("city.state")
            .with("accessories")
            .with("photos");
        })
        .first();
      return model;
    } catch (error) {
      return { error };
    }
  }
  async update({ params, request }) {
    try {
      const { id } = params;
      const data = request.except(["photo", "places"]);

      const slug = slugify(data.title, {
        replacement: "-", // replace spaces with replacement
        remove: null, // regex to remove characters
        lower: true // result in lower case
      });
      const model = await Model.find(id);
      await model.merge({ ...data, slug });
      await model.save();
      return model;
    } catch (error) {
      console.log(error);
      return { error };
    }
  }
  async upload({ request, params }) {
    try {
      const model = await Model.find(params.id);
      const validationOptions = {
        types: ["image"],
        size: "2mb",
        extnames: ["png", "gif", "jpg"]
      };
      const photo = request.file("photo", validationOptions);
      if (photo) {
        const randname = await Crypto.randomBytes(20).toString("hex");
        await photo.move(Helpers.publicPath("uploads/categories/"), {
          name: randname + "." + photo.extname,
          overwrite: true
        });
        await model.merge({
          image: `categories/${photo.fileName}`
        });
        await model.save();
      }
      return model;
    } catch (error) {
      console.log(error);
      return { error };
    }
  }
}

module.exports = CategoryController;
