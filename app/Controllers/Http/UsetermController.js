"use strict";
const Model = use("App/Models/UseTerm");
const Item = use("App/Models/UseTermsItem");
class UsetermController {
  async index() {
    try {
      const model = await Model.query()
        .with("items")
        .first();
      return model || {};
    } catch (error) {
      return { error };
    }
  }
  async update({ request }) {
    try {
      const data = request.all();
      const model = await Model.findOrCreate({
        id: 1
      });
      await model.merge(data);
      await model.save();

      return model;
    } catch (error) {
      console.log(error);
      return { error };
    }
  }
  async store({ request }) {
    try {
      const data = request.all();
      const model = await Item.create(data);

      return model;
    } catch (error) {
      return { error };
    }
  }
}

module.exports = UsetermController;
