"use strict";
const Model = use("App/Models/Message");
class MessageController {
  async index() {
    try {
      const model = await Model.all();
      return model;
    } catch (error) {
      return { error };
    }
  }
  async store({ request }) {
    try {
      const data = request.all();
      const model = await Model.create(data);

      return model;
    } catch (error) {
      return { error };
    }
  }
}

module.exports = MessageController;
