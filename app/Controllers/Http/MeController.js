"use strict";
const { format } = require("date-fns");
const Crypto = require("crypto");

const Model = use("App/Models/User");
const AdvertisingLocation = use("App/Models/AdvertisingLocation");

class MeController {
  async advertisings({ params }) {
    try {
      const model = await Model.find(params.id);

      const advertisings = await model
        .advertisings()
        .with("city.state")
        .with("accessories")
        .with("availabilities")
        .with("photos")
        .fetch();

      return advertisings;
    } catch (error) {
      return { error };
    }
  }
  async locations({ params }) {
    try {
      const model = await Model.find(params.id);

      const locations = await model
        .locations()
        .orderBy("created_at", "DESC")
        .with("advertising", builder => {
          builder
            .with("city.state")
            .with("accessories")
            .with("availabilities")
            .with("photos");
        })
        .fetch();

      return locations;
    } catch (error) {
      return { error };
    }
  }
  async confirmation_location({ params, request }) {
    try {
      const data = request.all();

      const reservation_code = await Crypto.randomBytes(36).toString("hex");

      const model = await AdvertisingLocation.create({
        advertising_id: data.advertising_id,
        date_end: data.date_end,
        date_init: data.date_init,
        hour_end: format(data.hour_end, "HH:mm"),
        hour_init: format(data.hour_init, "HH:mm"),
        occupants: data.occupants,
        price: data.price,
        price_total: data.price_total,
        reservation_code: reservation_code.substring(0, 20),

        user_id: params.id,
        paid: false
      });

      await model.merge({
        reservation_number: this.padDigits(model.toJSON().id, 19)
      });
      await model.save();
      return model;
    } catch (error) {
      return { error };
    }
  }
  async makepayment({ request, params }) {
    try {
      const data = request.all();

      const model = await AdvertisingLocation.query()
        .where("id", data.advertising_location)
        .where("user_id", params.id)
        .first();

      await model.merge({
        payment_status: "AUTHORIZED",
        paid: true
      });

      await model.save();
      return model;
    } catch (error) {
      return { error };
    }
  }

  padDigits(number, digits) {
    return (
      Array(Math.max(digits - String(number).length + 1, 0)).join(0) + number
    );
  }
}

module.exports = MeController;
