"use strict";
const Model = use("App/Models/User");
class UserController {
  async index() {
    try {
      const model = await Model.query().fetch();
      return model;
    } catch (error) {
      return { error };
    }
  }
  async store({ request }) {
    try {
      const data = request.except([
        "accpet_terms",
        "city",
        "password_confirmation",
        "state"
      ]);
      const model = await Model.create(data);
      return model;
    } catch (error) {
      console.log(error);
      return { error };
    }
  }
  async signup({ request, auth }) {
    try {
      const data = request.except([
        "accpet_terms",
        "city",
        "password_confirmation",
        "state"
      ]);
      const model = await Model.create(data);
      const { email } = model.toJSON();
      await auth.attempt(email, data.password);
      const user = await Model.query()
        .where("email", email)
        .first();
      const token = await auth.generate(user);
      return {
        user,
        token: token.token
      };
    } catch (error) {
      console.log(error);
      return { error };
    }
  }
  async show({ params }) {
    try {
      const { id } = params;
      const model = await Model.query()
        .where("id", id)

        .first();
      return model;
    } catch (error) {
      return { error };
    }
  }
  async update({ params, request }) {
    try {
      const { id } = params;
      const data = request.except([
        "password",
        "accpet_terms",
        "city",
        "password_confirmation",
        "state"
      ]);
      const model = await Model.find(id);
      await model.merge(data);
      await model.save();
      return model;
    } catch (error) {
      console.log(error);
      return { error };
    }
  }
  async signin({ request, auth }) {
    try {
      const { email, password } = request.all();
      await auth.attempt(email, password);
      const user = await Model.query()
        .where("email", email)
        .first();
      const token = await auth.generate(user);
      return {
        user,
        token: token.token
      };
    } catch (error) {
      console.log(error);
      return { error };
    }
  }
}

module.exports = UserController;
