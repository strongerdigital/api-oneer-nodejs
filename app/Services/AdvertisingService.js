var slugify = require("slugify");
const axios = require("axios");

class AdvertisingService {
  async createSlug(advertising) {
    return new Promise(function(resolve, reject) {
      const slug = slugify(advertising.title, {
        replacement: "-",
        remove: null,
        lower: true
      });
      resolve(`${slug}-${advertising.id}`);
    });
  }
  async geocode(advertising) {
    const street = advertising.street.replace(/ /g, "+");
    const city = advertising.city.replace(/ /g, "+");
    return new Promise(function(resolve, reject) {
      const url = `https://maps.googleapis.com/maps/api/geocode/json?address=${advertising.number}+${street},+${city},+${advertising.state}&key=AIzaSyBQDS5bsu4GQSzEooibZ3nVNbjR67bkneI`;
      axios.get(url).then(({ data }) => {
        resolve(data.results[0].geometry.location);
      });
    });
  }
}

module.exports = new AdvertisingService();
