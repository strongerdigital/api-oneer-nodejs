"use strict";

const Route = use("Route");

Route.group(() => {
  Route.get("/", "AccessoryController.index");
  Route.get("/:id", "AccessoryController.show");
  Route.post("/", "AccessoryController.store");
  Route.put("/:id", "AccessoryController.update");
}).prefix("/api/accessories");
