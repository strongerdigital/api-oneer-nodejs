"use strict";

const Route = use("Route");

Route.group(() => {
  Route.get("/", "CategoryController.index");
  Route.get("/:id", "CategoryController.show");
  Route.post("/", "CategoryController.store");
  Route.put("/:id", "CategoryController.update");
  Route.post("/:id/upload", "CategoryController.upload");
}).prefix("/api/categories");
