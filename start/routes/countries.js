"use strict";

const Route = use("Route");

Route.group(() => {
  Route.get("/", "CountryController.index");
  Route.get("/:id", "CountryController.show");
  Route.post("/", "CountryController.store");
  Route.put("/:id", "CountryController.update");
}).prefix("/api/countries");
