"use strict";

const Route = use("Route");

Route.group(() => {
  Route.get("/", "UsetermController.index");
  Route.post("/", "UsetermController.update");
}).prefix("/api/useterms");

Route.group(() => {
  Route.post("/", "UsetermController.store");
}).prefix("/api/usetermsitem");
