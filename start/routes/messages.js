"use strict";

const Route = use("Route");

Route.group(() => {
  Route.get("/", "MessageController.index");
  Route.get("/:id", "MessageController.show");
  Route.post("/", "MessageController.store");
  Route.put("/:id", "MessageController.update");
}).prefix("/api/messages");
