"use strict";

const Route = use("Route");

Route.group(() => {
  Route.post("/signin", "UserController.signin");
  Route.post("/signup", "UserController.signup");
}).prefix("/api/auth");

Route.group(() => {
  Route.get("/", "UserController.index");
  Route.get("/:id", "UserController.show");
  Route.post("/", "UserController.store");
  Route.put("/:id", "UserController.update");
}).prefix("/api/users");

Route.group(() => {
  Route.get("/advertisings", "MeController.advertisings");
  Route.get("/locations", "MeController.locations");
  Route.post("/confirmation_location", "MeController.confirmation_location");
  Route.post("/makepayment", "MeController.makepayment");
}).prefix("/api/users/:id");
