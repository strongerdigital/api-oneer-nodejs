"use strict";

const Route = use("Route");

Route.get("/", () => {
  return { greeting: "Hello world in JSON" };
});

require("./categories");
require("./advertisings");
require("./cities");
require("./accessories");
require("./user");
require("./countries");
require("./locations");
require("./messages");
require("./useterms");
