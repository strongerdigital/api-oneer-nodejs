"use strict";

const Route = use("Route");

Route.group(() => {
  Route.get("/", "AdvertisingController.index");
  Route.get("/:id", "AdvertisingController.show");
  Route.post("/:id/uploadPhoto", "AdvertisingController.uploadPhoto");
  Route.post("/", "AdvertisingController.store");
  Route.post("/search", "AdvertisingController.search");
  Route.get("/featured/list", "AdvertisingController.featured");
  Route.get("/last4/list", "AdvertisingController.last4");
  Route.put("/:id", "AdvertisingController.update");
  Route.post("/:id/availability", "AdvertisingController.availability");
  Route.post("/:id/accessory", "AdvertisingController.createAccessory");
}).prefix("/api/advertisings");
