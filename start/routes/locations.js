"use strict";

const Route = use("Route");

Route.group(() => {
  Route.get("/", "LocationController.index");
  Route.get("/:id", "LocationController.show");
  Route.post("/", "LocationController.store");
  Route.put("/:id", "LocationController.update");
}).prefix("/api/locations");
