"use strict";

const Route = use("Route");

Route.group(() => {
  Route.get("/", "CityController.index");
  Route.get("/:id", "CityController.show");
  Route.post("/", "CityController.store");
  Route.put("/:id", "CityController.update");
  Route.get("/search/:name/:uf", "CityController.search");
}).prefix("/api/cities");

Route.group(() => {
  Route.get("/", "StateController.index");
  Route.get("/:id", "StateController.show");
  Route.post("/", "StateController.store");
  Route.put("/:id", "StateController.update");
}).prefix("/api/states");
